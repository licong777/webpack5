import Vue from 'vue'
import VueRouter from 'vue-router'
// const path = require('path')
Vue.use(VueRouter)

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function (location) {
  return originalPush.call(this, location).catch(error => error)
}

const routerFiles = require.context('./modules', true, /\.js/)

const moduleRoutes = routerFiles.keys().reduce((prev, next) => {
  const value = routerFiles(next).default
  prev.push(value)
  return prev
}, [])

const routes = [
  {
    path: '/',
    redirect: '/Home'
  },
  ...moduleRoutes,
  {
    path: '*',
    redirect: '/Home'
  }
]

const createRouter = () => new VueRouter({
  routes,
  scrollBehavior: (to, from, savePosition) => {
    if (savePosition) {
      return savePosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

const router = createRouter()

export const resetRouter = () => {
  const crouter = createRouter()
  router.matcher = crouter.matcher
}

export default router
