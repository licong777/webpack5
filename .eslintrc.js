module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es6: true
  },
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  extends: [
    'plugin:vue/essential',
    'standard'
  ],
  plugins: [
    'vue'
  ],
  rules: { // 0-不验证  1-警告  2-错误
    'prefer-const': 0, // 不强制使用const去声明变量
    'no-var': 2, // 禁止使用var声明变量
    eqeqeq: 2, // 强制要求使用 === 和 !==
    'no-prototype-builtins': 0, // 允许直接使用Object.prototypes 的内置属性
    'array-callback-return': 2, // 强制数组方法的回调函数中有 return 语句
    'default-case': 2 // 要求 switch 语句中有 default 分支
  }
}
