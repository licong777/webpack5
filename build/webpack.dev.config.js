const webpack = require('webpack')
const path = require('path')
const { merge } = require('webpack-merge')
const base = require('./webpack.base.config')
const resolve = (dir) => path.resolve(__dirname, dir)

module.exports = merge(base, {
  mode: 'development',
  devtool: 'eval-cheap-module-source-map',
  devServer: {
    host: 'localhost',
    port: 8080,
    open: true,
    hot: true,
    compress: true,
    historyApiFallback: true,
    contentBase: resolve('../dist'),
    proxy: {
      '/api': {
        target: 'http://localhost:8080',
        secure: false,
        changeOrigin: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ]
})
