const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const { VueLoaderPlugin } = require('vue-loader')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const CompressionWebpackPlugin = require('compression-webpack-plugin')
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')
const notifier = require('node-notifier')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')

const devMode = process.env.NODE_ENV === 'development'
const resolve = (dir) => path.resolve(__dirname, dir)

module.exports = {
  entry: [
    '@babel/polyfill',
    resolve('../src/main.js')
  ],
  output: {
    path: resolve('../dist'),
    filename: 'js/[name].js',
    chunkFilename: 'js/[name].js',
    clean: true
  },
  stats: {
    preset: 'minimal',
    builtAt: true,
    errorDetails: true,
    moduleTrace: true
  },
  externals: {
    vue: 'Vue',
    'vue-router': 'VueRouter',
    vuex: 'Vuex',
    axios: 'axios',
    lodash: '_',
    'element-ui': 'ELEMENT'
  },
  performance: {
    hints: 'warning',
    maxEntrypointSize: 5 * 1024 * 1024,
    maxAssetSize: 5 * 1024 * 1024,
    assetFilter: function (assetFilename) {
      return assetFilename.endsWith('.js') || assetFilename.endsWith('.css')
    }
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        exclude: [resolve('../node_modules')],
        use: ['vue-loader']
      },
      {
        test: /\.jsx?$/,
        exclude: [resolve('../node_modules')],
        use: [
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true
            }
          }
        ]
      },
      // {
      //   test: /\.(jsx?|vue)$/,
      //   use: ['eslint-loader'],
      //   // enforce: 'pre',
      //   exclude: [resolve('../node_modules')]
      // },
      {
        test: /\.(css|less)$/,
        exclude: [resolve('../node_modules')],
        use: [
          devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
          'less-loader',
          {
            loader: 'sass-resources-loader',
            options: {
              resources: [
                resolve('../src/assets/styles/common.less')
              ]
            }
          }

        ]
      },
      {
        test: /\.(png|jpg|jpeg|svg|gif)$/,
        type: 'asset/inline'
      },
      {
        test: /\.(ttf|otf|woff2?|eot)$/,
        type: 'asset/resource',
        generator: {
          filename: 'fonts/[name][ext]'
        }
      }
    ]
  },
  resolve: {
    extensions: ['.vue', '.js', '.less'],
    alias: {
      '@': resolve('../src')
    },
    fallback: {
      path: require.resolve('path-browserify')
    }
  },
  plugins: [
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      template: resolve('../public/index.html'),
      favicon: resolve('../public/lyf.jpg')
    }),
    new MiniCssExtractPlugin({
      filename: 'css/[name].css'
    }),
    new webpack.ProgressPlugin(),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: resolve('../src/statics/dependences'),
          to: resolve('../dist/dependences')
        }
      ]
    }),
    new CompressionWebpackPlugin({
      test: /\.(js|css)$/
    }),
    new FriendlyErrorsWebpackPlugin({
      onErrors: (severity, errors) => {
        if (severity !== 'error') {
          return
        }
        const error = errors[0]
        notifier.notify({
          title: 'Webpack error',
          message: severity + ': ' + error.name,
          subtitle: error.file || ''
        })
      }
    }),
    new BundleAnalyzerPlugin({
      analyzerMode: 'disabled',
      generateStatsFile: true
    })
  ]
}
